'use strict';

angular.module('inasapp.nasas', ['ngRoute', 'inasapp.nasas.service'])
.config(
  ['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {

  $routeProvider
  .when('/nasas', {
    templateUrl: 'nasas/nasas.html',
    controller: 'NasasCtrl',
    resolve: { nasasService: ['NasasResource', function( NasasResource ){ return NasasResource.all(); }] }
  })
  .when('/nasas/:nasaId', {
    templateUrl: 'nasas/details.html',
    controller: 'NasaDetailsCtrl',
    resolve: { nasaService: 
    	['NasasResource', '$route', 
    	function( NasasResource, $route ){ 
    		return NasasResource.get($route.current.params.nasaId)
    	}]
    }
  });

}])
.controller('NasasCtrl', ['$scope', 'nasasService', '$compile', function($scope, nasasService, $compile) {
	$scope.nasas = nasasService
	console.log(nasasService)
	$scope.elementActive = 0
    
    var nasotas = {}
    for(var i = 0 ; i < nasasService.length ; i++){
    	nasotas[nasasService[i].id] = {
    		lat: nasasService[i].gps.lat,
        	lng: nasasService[i].gps.lng,
            message: nasasService[i].name
            /*icon: local_icons.default_icon,*/
    	}
    }
    angular.extend($scope, {
    	vigo:{
    		lat: 42.2261474,
    		lng: -8.7604044,
    		zoom: 11
    	},
    	markers: nasotas,
        defaults: {
            scrollWheelZoom: false
        }
    })

    $scope.$on('leafletDirectiveMarker.click', function(event, args){
        console.log('event click %O',args)
        var markerName = args.model.message; //has to be set above
		var $container = $(args.leafletEvent.target._popup._container).find('.leaflet-popup-content-wrapper'); 
		$container.empty();
		var html = "<p> "+markerName +" </p>", 
		linkFunction = $compile(angular.element(html)),             
		linkedDOM = linkFunction($scope); 
		$container.append(linkedDOM);

		$scope.elementActive = args.modelName
    });


	console.log($scope.markers)

}])
.controller('NasaDetailsCtrl', ['$scope', 'nasaService', 'NasasResource', '$route', 
	function($scope, nasaService, NasasResource, $route) {

	$scope.nasa = nasaService
	console.log(nasaService)
	
	var marks = {}
	marks[nasaService.id] = {
	    lat: nasaService.gps.lat,
    	lng: nasaService.gps.lng,
        message: nasaService.name,
    }

    NasasResource.getHist($route.current.params.nasaId).then(function(response){
    	console.log('history received  : %O',response)
    	nasaService['history'] = response

    	console.log(nasaService)
    })

	angular.extend($scope, {
		vigo:{
    		lat: 42.2261474,
    		lng: -8.7604044,
    		zoom: 11
    	},
        markers: marks,
        defaults: {
            scrollWheelZoom: false
        }
    })

}])