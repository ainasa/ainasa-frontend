'use strict';

// Declare app level module which depends on views, and components
angular.module('inasapp', 
  [
  'ngRoute',
  'ngResource',
  'ngAnimate',
  'ui.bootstrap',
  'leaflet-directive',
  'inasapp.nasas',
  'angular-loading-bar'
  ]
)
.config(
  ['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.otherwise({
      redirectTo: '/nasas'
    });
}])
.controller("ButtonsCtrl", ['$scope', function($scope){
	$scope.singleModel = 1;

	$scope.radioModel = 'Middle';

	$scope.checkModel = {
	left: false,
	middle: true,
	right: false
	};

	$scope.checkResults = [];

	$scope.$watchCollection('checkModel', function () {
		$scope.checkResults = [];
		angular.forEach($scope.checkModel, function (value, key) {
			if (value) {
				$scope.checkResults.push(key);
			}
		});
	});
}])
.controller('CarouselDemoCtrl',['$scope', function($scope){
	$scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;
  var slides = $scope.slides = [];
  var currIndex = 0;

  $scope.addSlide = function() {
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: 'http://lorempixel.com/' + newWidth + '/300',
      text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
      id: currIndex++
    });
  };

  $scope.randomize = function() {
    var indexes = generateIndexesArray();
    assignNewIndexesToSlides(indexes);
  };

  for (var i = 0; i < 4; i++) {
    $scope.addSlide();
  }

  // Randomize logic below

  function assignNewIndexesToSlides(indexes) {
    for (var i = 0, l = slides.length; i < l; i++) {
      slides[i].id = indexes.pop();
    }
  }

  function generateIndexesArray() {
    var indexes = [];
    for (var i = 0; i < currIndex; ++i) {
      indexes[i] = i;
    }
    return shuffle(indexes);
  }

  // http://stackoverflow.com/questions/962802#962890
  function shuffle(array) {
    var tmp, current, top = array.length;

    if (top) {
      while (--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
      }
    }

    return array;
  }
}])
.controller('MainCtrl',['$rootScope', function($rootScope){
  
}])
