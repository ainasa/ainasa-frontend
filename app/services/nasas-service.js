'use strict';

angular.module('inasapp.nasas.service', [])
.factory('NasasResource', ['$resource', function($resource) {

	var path = 'http://172.16.3.125:5000/nasa';
	var nasasResource = $resource(path+'/:id',{id:'@id'});
	var nasasHistoryResource = $resource(path+'-history/?nasa_id=:id',{id:'@id'});

	var factory = {};

	factory.all = function() {
		return nasasResource.get().$promise.then(function(response){
			
			if( response.data.length == 0 ){
				return $resource('json/nasas:id.json',{id:'@id'}).query().$promise
			}
			return response.data
		},function(err){
			return $resource('json/nasas:id.json',{id:'@id'}).query().$promise;
		})
	};

	factory.get = function(id) {
		return nasasResource.get({'id':id}).$promise.then(function(response){
			return response
		},function(err){
			return $resource('json/nasas.:id.json',{id:'@id'}).get({'id':id}).$promise;
		})
	};
	
	factory.getHist = function(id){
		return nasasHistoryResource.get({'id':id}).$promise.then(function(response){
			return response.data
		})
	};

	return factory;
}]);