# angular-seed — the seed for AngularJS apps

# compile css
```
sass --watch app.scss:app.css 
```

# serve web
```
python3 -m http.server
```

# notes about location GPS
```
sudo kextunload /System/Library/Extensions/HoRNDIS.kext

UTC timer 13-33-28
latitude =  4213.9088, longitude =   844.0434
satellites number = 8
LGPS loop
$GPGGA,133330.000,4213.9088,N,00844.0434,W,1,8,1.10,26.8,M,52.0,M,,*7A

UTC timer 13-33-30
latitude =  4213.9088, longitude =   844.0434
satellites number = 8
LGPS loop
$GPGGA,133332.000,4213.9088,N,00844.0434,W,1,8,1.10,26.8,M,52.0,M,,*78

UTC timer 13-33-32
latitude =  4213.9088, longitude =   844.0434
satellites number = 8


8.44 / 60
E + W -
N + S -
```

## Contact

For more information on AngularJS please check out http://angularjs.org/

[git]: http://git-scm.com/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://jasmine.github.io
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
[http-server]: https://github.com/nodeapps/http-server
